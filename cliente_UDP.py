#!/usr/bin/env python2.7
import socket #importa el paquete de socket

host_ip = "localhost" #Direccion IP del host es 127.0.0.1
host_port = 5007 #Puerto del host
host = host_ip #La direccion IP del cliente es igual a la del host
port = 5008# El puerto del Host

print "---------------------------------------------------"
print "-------------------Cliente UDP---------------------"
print "---------------------------------------------------"
print "Iniciando...."

cliente = socket.socket( #Se crea el socket del cliente
            socket.AF_INET, #Se indica que son direcciones de tipo IP v4
            socket.SOCK_DGRAM) #Se indica que el tipo de socket es de Datagrama
cliente.bind((host,port)) #Se liga al socket del cliente la direccion ip y el puerto del cliente para poder recivir mensajes del host
print "Enviando Mesnaje al Servidor UDP:"
print "IP: "+host_ip
print "Puerto: "+str(host_port)

mensaje = raw_input("Escriba el memsaje a enviar al Servidor> ") # Se escribe el mensaje que se quiere mandar al servidor

while mensaje != '': #Ciclo while principal donde si el mensaje que se envia esta en blanco, se cierra el socket del cliente
    print "Mensaje Enviado."
    cliente.sendto(mensaje, (host_ip, host_port)) #Se envia el mensaje a la dirrecion ip y puerto del servidor
    print "Recibiendo Respuesta..."
    respuesta,addr = cliente.recvfrom(1024) #Se recibe una respuesta del host con un tamano maximo de 1024 bytes
    print "El Servidor UDP envia: "
    print respuesta
    mensaje = raw_input("Escriba el memsaje a enviar al Servidor> ") # Se vuelve a enviar el mensaje al servidor
    if not mensaje: #Si el mensaje viene en blanco se envia al servidor puesto que el ciclo va a terminar y mensaje no se enviaria para que se cierre el Servidor
        cliente.sendto(mensaje, (host_ip, host_port)) #Se envia el mensaje en blanco al servidor
print "Se envio mensaje en blanco. Cerrando el Cliente..."
cliente.close() #Se cierra el servidor
