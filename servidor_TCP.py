#!/usr/bin/env python2.7
import socket  #importa el paquete de socket


host = 'localhost' #Direccion IP del servidor, es la 127.0.0.1
port = 5007 #Puerto del Servidor

print "---------------------------------------------------"
print "-------------------Servidor TCP--------------------"
print "---------------------------------------------------"
print "Iniciando...."
servidor = socket.socket( #Se crea el socket de el servidor de Bienvenida
            socket.AF_INET, #La direccion es de Tipo IP v4
            socket.SOCK_STREAM) #El socket es de tipo con conexion
servidor.bind((host, port)) #Se le liga al socket el puerto y la direccion
print "IP: "+host
print "Puerto: "+str(port)

print "Esperando Conexion....."
servidor.listen(1) #Se pone al socket del servidor a escuchar, con una cola de 1 conexion
print "Buscando 1 conexion....."
cliente, addr = servidor.accept() #Si se recibe una iniciacion de 3 way Handshake, se acepta la conexion
#Este metodo devuelve un socket cliente por el cual se pueden enviar y recibir mensajes con el cliente conectado
print "Conexion Aceptada:" #En la tupla addr se tienen el puerto y la direccion ip del cliente conenctado
print "IP: "+str(addr[0])
print "Puerto: "+str(addr[1])

while True: #Se inicia el ciclo donde se reciben y envian mensajes al cliente, Si el mensaje esta en blanco se para el ciclo
    print "Recibiendo datos del cliente conectado..."
    mensaje = cliente.recv(1024) #Se recibe el mensaje del cliente, con un tamano maximo del buffer de 1024 bytes

    if not mensaje: #Si el mensaje viene en blanco, que salga del ciclo
        break
    print "Mensaje Recibido: " +mensaje
    mensaje_nuevo = mensaje.upper() #Se utiliza el metodo upper para devolver el mensaje totalmente en mayusculas
    print "Mensaje Cambiado enviado al cliente: "+mensaje_nuevo
    cliente.send(mensaje_nuevo) #Se envia el mensaje al cliente de vuelta

print "No se envio ningun mensaje. Cerrando conexion con ..."
cliente.close() #Se cierra el socket auxiliar del cliente abierto
servidor.close()# Se cierra el socket de bienvenida del servidor
