#!/usr/bin/env python2.7s
import socket #importa el paquete de socket

host_ip = "localhost" # Direccion IP del cliente, es la 127.0.0.1
port = 5008 #Puerto del Cliente
host_port = 5007 #Puerto del Servidor

print "---------------------------------------------------"
print "-------------------Cliente TCP---------------------"
print "---------------------------------------------------"
print "Iniciando...."

cliente = socket.socket( #Se crea el socket del cliente
        socket.AF_INET,  #Se indica que se van a utilizar direcciones IP v4
        socket.SOCK_STREAM) #Se que es un socket de tipo con conexion
cliente.bind((host_ip,port)) # Se le liga al socket el puerto la direccion Ip del cliente
print "IP: "+host_ip
print "Puerto: "+str(port)

print "Conectando con Servidor TCP, Iniciando 3-way Hanshake:"
cliente.connect((host_ip,host_port)) # Se inicia el 3'way Handshake con el Servidor
print "Recibido SYN del Servidor, Realizando ACK."


mensaje = raw_input("Escriba el memsaje a enviar al Servidor> ") # Se le pide al cliente el mensaje a enviar
print "Mensaje Enviado."
while mensaje != '': #Si el mensaje a enviar no esta en blanco, proceda a enviar y recibir del Servidor
    cliente.send(mensaje) # Se envia el mensaje al servidor una vez conectado
    print "Recibiendo Respuesta..."
    respuesta = cliente.recv(1024) #Se recibe el mensaje del servidor con un tamano maximo de 1024 bytes
    print "El Servidor TCP envia: "
    print respuesta #Se imprime la respuesta del sevidor
    mensaje = raw_input("Escriba el memsaje a enviar al Servidor> ") # Se vuelve a pedir al usuario un mensaje
    if not mensaje: # Si el mensaje esta en blanco, se envia porque esto va a terminar el ciclo y no se enviaria por defecto
        cliente.send(mensaje)
print "No se envio ningun mensaje. Cerrando conexion con el servidor..."
cliente.close() #Se cierra la conexion con el servidor
