#!/usr/bin/env python2.7
import socket  #importa el paquete de socket


host = 'localhost' #Direccion IP del Servidor, es 127.0.0.1
port = 5007 #Puerto del Servidor

print "---------------------------------------------------"
print "-------------------Servidor UDP--------------------"
print "---------------------------------------------------"
print "Iniciando...."
servidor = socket.socket( #Se crea el socket del servidor listo para enviar y recibir
            socket.AF_INET, # Se indica que son direcciones IP v4
            socket.SOCK_DGRAM) # Se indica que el tipo de socket es datagrama (sin conexion)
servidor.bind((host,port)) # Se liga al socket la direccion ip y el puerto del servidor

print "IP: "+host
print "Puerto: "+str(port)

while True: # Ciclo princial, cuando se tiene un mensaje en blanco se finaliza y se cierra el socket del Servidor
    print "Recibiendo mensaje del cliente...."
    mensaje, addr = servidor.recvfrom(1024) #Se recibe un mensaje del cliente y su direccion ip con su puerto, el cual el mensaje tiene un tamano maximo de 1024
    client_port = addr[1] # Se guarda e puerto del servidor para enviarle el mensaje procesado
    if not mensaje: #Si el mensaje recibido esta en blanco, se sale del ciclo
        break
    print "Mensaje Recibido: " +mensaje
    mensaje_nuevo = mensaje.upper() # Se utiliza el metodo upper() para poner en mayuscula todas las letras de el mensaje recibido
    print "Mensaje Cambiado enviado al cliente: "+mensaje_nuevo
    servidor.sendto(mensaje_nuevo,(host,client_port)) # Se envia el mensaje al cliente utilizan la direccion IP que comparten y el puerto del cliente

print "Se recibio mensaje en blanco. Cerrando Servidor..."
servidor.close() #Se cierra el socket del servidor una vez que se recibio el mensaje en blanco
