# Parte 2: Programacion con Sockets

Parata esta parte se realizaron 4 programas que trabajan en parejas:

* servidor_TCP.py con cliente_TCP.py
* servidor_UDP.py con cliente_UDP.py

Para terminar la ejecucion tanto del servidor como el cliente para los dos protocolos, el cliente debe de enviar un mensaje en blanco (nada mas apretar la tecla enter sin escribir nada). Se debe de correr estos programas usando Python 2.7 .

## Ejecucion de los Programas

Para cada par de programas, Se tienen que abrir **dos terminarles separadas**. En la primera terminal se debe de ejecutar primero el programa servidor y en la segunda terminal ejecutar el programa cliente para los dos tipos de protocolos de la transporte.


### Cliente-Servidor TCP

Se ejecuta en la primera terminal:

```bash
python servidor_TCP.py
```

En la segunda terminal se debe de usar el siguiente comando

```bash
python cliente_TCP.py
```

### Cliente-Servidor UDP

Se ejecuta en la primera terminal:

```bash
python servidor_UDP.py
```

En la segunda terminal se debe de usar el siguiente comando

```bash
python cliente_UDP.py
```
Si se desearan ejecutar en una sola terminal, se debe de enviar la ejecucion del programa servidor a segundo plano y despues ejecutar el cliente. Esto se hacer para cada tipo de protocolo con los siguientes comandos:

```bash
python servidor_TCP.py &
python cliente_TCP.py
```

```bash
python servidor_UDP.py &
python cliente_UDP.py
```
